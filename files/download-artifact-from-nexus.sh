#!/bin/bash

# Define Nexus Configuration
NEXUS_BASE=
REST_PATH=/service/local
ART_REDIR=/artifact/maven/redirect

usage()
{
cat <<EOF

usage: $0 options

This script will fetch an artifact from a Nexus server using the Nexus REST redirect service.

OPTIONS:
   -h    Show this message
   -v    Verbose
   -a    GAV coordinate groupId:artifactId:version
   -c    Artifact Classifier
   -e    Artifact Packaging
   -o    Output file
   -r	 Repository
   -s    Region
   -n    Nexus Base URL

EOF
}

# Read in Complete Set of Coordinates from the Command Line
GROUP_ID=
ARTIFACT_ID=
VERSION=
CLASSIFIER=""
PACKAGING=jar
REPO=
REGION="us-west-2"
VERBOSE=0

OUTPUT=

while getopts "hva:c:e:o:r:s:n:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         a)
	     	 OIFS=$IFS
             IFS=":"
		     GAV_COORD=( $OPTARG )
		     GROUP_ID=${GAV_COORD[0]}
             ARTIFACT_ID=${GAV_COORD[1]}
             VERSION=${GAV_COORD[2]}	     
	    	 IFS=$OIFS
             ;;
         c)
             CLASSIFIER=$OPTARG
             ;;
         e)
             PACKAGING=$OPTARG
             ;;
         v)
             VERBOSE=1
             ;;
		 o)
			OUTPUT=$OPTARG
			;;
		 r)
		    REPO=$OPTARG
		    ;;
		 s)
		    REGION=$OPTARG
		    ;;			
		 n)
			NEXUS_BASE=$OPTARG
			;;
         ?)
             echo "Illegal argument $OPTION=$OPTARG" >&2
             usage
             exit
             ;;
     esac
done

if [[ -z $GROUP_ID ]] || [[ -z $ARTIFACT_ID ]] || [[ -z $VERSION ]]
then
     echo "BAD ARGUMENTS: Either groupId, artifactId, or version was not supplied" >&2
     usage
     exit 1
fi

# Define default values for optional components

# If we don't have set a repository and the version requested is a SNAPSHOT use snapshots, otherwise use releases
if [[ "$REPOSITORY" == "" ]]
then
	if [[ "$VERSION" =~ ".*SNAPSHOT" ]]
	then
		: ${REPO:="snapshots"}
	else
		: ${REPO:="releases"}
	fi	
fi

# Output
OUT="."
if [[ "$OUTPUT" != "" ]] 
then
  OUT=$(mktemp -d)
fi

artifactKey="$REPO/${GROUP_ID//.//}/$ARTIFACT_ID/$VERSION"
patchNumber=$(aws s3 --region $REGION ls s3://$NEXUS_BASE/$artifactKey --recursive | grep -o "[0-9]*.$PACKAGING$" | grep -o "[0-9]*" | sort -gr | head -1)
artifactIdPattern="$artifactKey/$ARTIFACT_ID*$patchNumber.$PACKAGING"

echo "Fetching Artifact $ARTIFACT_ID using $artifactIdPattern pattern..." >&2
aws s3 --region $REGION cp s3://$NEXUS_BASE $OUT --recursive --exclude "*" --include "$artifactIdPattern"
mv $OUT/$artifactIdPattern $OUTPUT